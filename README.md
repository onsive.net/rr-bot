First **pre alpha** release for the public.

Please don't blame me for the design and/or for errors,
this repo was crafted in less then 3 hours.

Please do not start contributing whole code peaces right now,
as I have a clear plan of what I want to reach.

You can start contributing in mass in the pre beta *(no release date)*

*Minor issues can be pointed out in every stage ;)*

**[Download Portable Version](https://nc.onsive.net/index.php/s/siekW2TkZxNgcHA)** `74.6 MB` `OnSive.RR.Bot-1.0.0-preAlpha+0x0002.zip`

### What is already implemented
- Fast user switching for multiple RR accounts
- Whole homepage functionality
- <s>Bot features</s>

### Requirements
- .net 4.8
- x64 architecture
- internet connection

### Getting Started
1. Check the requirements
2. Download the portable version file
3. Unzip the .zip file
4. Run `OnSive.RR.Bot.exe`
5. Add a new user *(username can be choosen freely, right now it got just an internal use)*
6. Click `Save`
6. Click `Login` next to your username
7. Log in with your provider of choice *(the cookies are stored under `cache\{username}\`)*