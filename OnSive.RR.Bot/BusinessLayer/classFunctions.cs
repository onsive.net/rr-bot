﻿using CefSharp.Wpf;
using OnSive.RR.Bot.Common.Functions;
using OnSive.RR.Bot.Common.Variables;
using Serilog;
using System;
using System.Linq;
using System.Windows;

namespace OnSive.RR.Bot.BusinessLayer
{
    public static class classFunctions
    {
        public static void Load_RR_Url(this ChromiumWebBrowser WebBrowser, string sSubPath)
        {
            try
            {
                string sURL = classGlobalFunctions.Get_Url(sSubPath);
                Log.Debug("Loading URL {0}", sURL);
                WebBrowser.Load(sURL);
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        public static void Prepare_Window(this Window wnd)
        {
            try
            {
                // Sets the window properties
                wnd.Title = classGlobalVariables.sTitle;
                wnd.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }
    }
}
