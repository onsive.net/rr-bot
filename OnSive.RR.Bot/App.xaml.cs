﻿using OnSive.RR.Bot.Common.Functions;
using OnSive.RR.Bot.Common.Variables;
using OnSive.RR.Bot.PrasentationLayer.Windows;
using Serilog;
using Serilog.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace OnSive.RR.Bot
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Initializes the Logger
            Log.Logger = new LoggerConfiguration()
#if DEBUG
                    .MinimumLevel.Debug()
#else
                    .MinimumLevel.Information()
#endif
                    .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
                .Enrich.WithExceptionDetails()
                .WriteTo.File(new Serilog.Formatting.Compact.CompactJsonFormatter(),
                              "logs\\.txt",
                              rollingInterval: RollingInterval.Day,
                              restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning)
                .WriteTo.Telegram("1049423383:AAEKyjGdw0Ra2bYI1cG7mj4o55WXyI8X0VU",
                                  chatId: "-1001448768515",
                                  restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error,
                                  period: new TimeSpan(0, 0, 10))
                .CreateLogger();

            try
            {
                // Init Console
                Console.Title = classGlobalVariables.sTitle;
                classGlobalFunctions.RePosition_Window(55, 0);
                NativeMethods.AllocConsole();

                // Log
                Log.Information(classGlobalVariables.sTitle);
                Log.Information("Startup ...");

                // Init Main Window
                this.MainWindow = new wpfMainWindow();
                this.MainWindow.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }
    }
}
