﻿using System;
using System.Linq;
using System.Reflection;

namespace OnSive.RR.Bot.Common.Variables
{
    public static class classGlobalVariables
    {
        private const string sPrerelease = "preAlpha";
        private const string sBuild = "0x0002";

        public static string sTitle
        {
            get
            {
                return ($"{Assembly.GetExecutingAssembly().GetName().Version.ToString(3)}") +
                    ($"{(string.IsNullOrEmpty(sPrerelease) ? string.Empty : ("-" + sPrerelease))}") +
                    ($"{(string.IsNullOrEmpty(sBuild) ? string.Empty : ("+" + sBuild))}") +
                    ($" | OnSive | RR Bot");
            }
        }

        public static string sUser { get; set; }
    }
}
