﻿using OnSive.RR.Bot.Common.Structures;
using Serilog;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace OnSive.RR.Bot.Common.Functions
{
    public static class classGlobalFunctions
    {
        /// <summary>
        /// Returns the url of a section
        /// </summary>
        public static string Get_Url(string sSubUrl)
        {
            try
            {
                // Check
                if (string.IsNullOrWhiteSpace(sSubUrl))
                {
                    // Error
                    throw new ArgumentException("Empty key passed!", nameof(sSubUrl));
                }
                // Combines the basic Url and its path
                return Resources.URL._basic_ + sSubUrl;
            }
            catch (Exception ex)
            {
                Log.Error(ex, Get_CurrentMethod());
                return string.Empty;
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string Get_CurrentMethod()
        {
            try
            {
                // Gets the caller method
                MethodBase MethodBase_PrevMethod = new StackTrace().GetFrame(1).GetMethod();

                // Returns the whole path
                return $"{MethodBase_PrevMethod.DeclaringType.FullName}.{MethodBase_PrevMethod.Name}";
            }
            catch (Exception ex)
            {
                Log.Error(ex, "OnSive.RR.Bot.Common.Functions.classGlobalFunctions.Get_CurrentMethod");
                return string.Empty;
            }
        }

        public static void Hide_ScriptErrors(WebBrowser WebBrowser_Tmp, bool bHide)
        {
            FieldInfo FieldInfo_ComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (FieldInfo_ComWebBrowser == null)
            {
                return;
            }

            object oComWebBrowser = FieldInfo_ComWebBrowser.GetValue(WebBrowser_Tmp);
            if (oComWebBrowser == null)
            {
                WebBrowser_Tmp.Loaded += (o, s) => Hide_ScriptErrors(WebBrowser_Tmp, bHide); //In case we are to early
                return;
            }
            oComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, oComWebBrowser, new object[] { bHide });
        }

        public static void RePosition_Window(int x, int y)
        {
            try
            {
                // Gets the console Interpreter
                IntPtr IntPtr_WindowHandle = NativeMethods.GetConsoleWindow();

                // Init struct
                RECT stRect = new RECT();

                // Fills the struct
                NativeMethods.GetWindowRect(IntPtr_WindowHandle, ref stRect);

                // Moves the window to its new position
                NativeMethods.MoveWindow(IntPtr_WindowHandle, x, y, stRect.right - stRect.left, stRect.bottom - stRect.top, true);
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }
    }
}
