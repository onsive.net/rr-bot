﻿using OnSive.RR.Bot.BusinessLayer;
using OnSive.RR.Bot.Common.Functions;
using OnSive.RR.Bot.Common.Variables;
using OnSive.RR.Bot.Properties;
using Serilog;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace OnSive.RR.Bot.PrasentationLayer.Windows
{
    /// <summary>
    /// Interaktionslogik für wpfUserSelection.xaml
    /// </summary>
    public partial class wpfUserSelection : Window
    {
        public wpfUserSelection()
        {
            try
            {
                InitializeComponent();

                this.Prepare_Window();

                if (Settings.Default.Users == null)
                {
                    Settings.Default.Users = new StringCollection();
                    Button_AddUser_Click(this, null);
                }
                else
                {
                    Load_Users();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Load_Users()
        {
            try
            {
                // Removes all user buttons
                StackPanel_Users.Children.Clear();

                // Foreach user
                foreach (string sUser in Settings.Default.Users)
                {
                    Label Label_Username = new Label();
                    Label_Username.Width = 210;
                    Label_Username.HorizontalContentAlignment = HorizontalAlignment.Right;
                    Label_Username.Content = sUser;

                    Button Button_Login = new Button();
                    Button_Login.Width = 200;
                    Button_Login.Content = "Login";
                    Button_Login.Tag = sUser;
                    Button_Login.Margin = new Thickness(10, 0, 10, 0);
                    Button_Login.Click += Button_Login_Click;

                    Button Button_Edit = new Button();
                    Button_Login.Width = 100;
                    Button_Edit.Content = "Edit";
                    Button_Edit.Tag = sUser;
                    Button_Edit.Click += Button_Edit_Click;

                    StackPanel StackPanel_UserTmp = new StackPanel();
                    StackPanel_UserTmp.Orientation = Orientation.Horizontal;
                    StackPanel_UserTmp.Children.Add(Label_Username);
                    StackPanel_UserTmp.Children.Add(Button_Login);
                    StackPanel_UserTmp.Children.Add(Button_Edit);

                    StackPanel_Users.Children.Add(StackPanel_UserTmp);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Button_Edit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string sUser = ((Button)sender).Tag.ToString();
                TextBox_Name.IsEnabled = false;
                TextBox_Name.Text = sUser;

                Grid_EditUser.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                classGlobalVariables.sUser = ((Button)sender).Tag.ToString();
                this.Close();
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Button_AddUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Removes the text in the textbox
                TextBox_Name.IsEnabled = true;
                TextBox_Name.Text = string.Empty;

                // Opens the popup
                Grid_EditUser.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // There's no edit functionality right now
                if (TextBox_Name.IsEnabled == false)
                {
                    // Closes the popup
                    Grid_EditUser.Visibility = Visibility.Collapsed;
                    return;
                }
                // Check
                if (Settings.Default.Users.Contains(TextBox_Name.Text))
                {
                    // Error
                    MessageBox.Show("Username already exists!", classGlobalVariables.sTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
                    Log.Warning("Username {0} already exists!", TextBox_Name.Text);
                    return;
                }
                // Adds the user
                Settings.Default.Users.Add(TextBox_Name.Text);

                // Saves the new settings
                Settings.Default.Save();

                Log.Information("User {0} was successfully created!", TextBox_Name.Text);

                // Closes the popup
                Grid_EditUser.Visibility = Visibility.Collapsed;

                Load_Users();
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Check
                if (MessageBox.Show($"Delete user {TextBox_Name.Text}?", classGlobalVariables.sTitle, MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                {
                    // Cancle
                    return;
                }

                // Check
                if (Settings.Default.Users.Contains(TextBox_Name.Text))
                {
                    // Error
                    MessageBox.Show("User already removed.", classGlobalVariables.sTitle, MessageBoxButton.OK, MessageBoxImage.Information);
                    Log.Warning("User {0} already removed.", TextBox_Name.Text);
                    return;
                }

                DirectoryInfo di = new DirectoryInfo($"cache\\{TextBox_Name.Text}");

                if (di.Exists)
                {
                    if (Directory.Exists("cache-backup") == false)
                    {
                        Directory.CreateDirectory("cache-backup");
                    }
                    di.MoveTo($"cache-backup\\{DateTime.Now:yyyy.MM.dd-HH:mm:ss}-{TextBox_Name.Text}");
                }

                // Adds the user
                Settings.Default.Users.Remove(TextBox_Name.Text);

                // Saves the new settings
                Settings.Default.Save();

                // Closes the popup
                Grid_EditUser.Visibility = Visibility.Collapsed;

                // Log
                Log.Debug("Cache backup created");
                Log.Information("User {0} successfully was removed.", TextBox_Name.Text);

                Load_Users();
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }
    }
}
