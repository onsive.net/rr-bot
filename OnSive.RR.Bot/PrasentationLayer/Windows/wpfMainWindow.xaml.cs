﻿using CefSharp;
using CefSharp.Wpf;
using OnSive.RR.Bot.BusinessLayer;
using OnSive.RR.Bot.Common.Functions;
using OnSive.RR.Bot.Common.Variables;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Windows;

namespace OnSive.RR.Bot.PrasentationLayer.Windows
{
    /// <summary>
    /// Interaktionslogik für wpfMain.xaml
    /// </summary>
    public partial class wpfMainWindow : Window
    {
        public wpfMainWindow()
        {
            try
            {
                Log.Information("User selection");

                wpfUserSelection UserSelection = new wpfUserSelection();
                UserSelection.ShowDialog();

                if (string.IsNullOrEmpty(classGlobalVariables.sUser))
                {
                    Log.Information("No user was selected");
                    Log.Information("Exiting");

                    Environment.Exit(0);
                    return;
                }

                Log.Information("User {0} was selected", classGlobalVariables.sUser);

                if (Directory.Exists("cache") == false)
                {
                    Directory.CreateDirectory("cache");
                }

                // Init WebBrowser Settings
                CefSettings Settings = new CefSettings();

                // Increase the log severity so CEF outputs detailed information, useful for debugging
                Settings.LogSeverity = LogSeverity.Warning;

                // By default CEF uses an in memory cache, to save cached data e.g. passwords you need to specify a cache path
                // NOTE: The executing user must have sufficient privileges to write to this folder.
                Settings.CachePath = $"cache\\{classGlobalVariables.sUser}";

                // Init WebBrowser
                Cef.Initialize(Settings);

                InitializeComponent();

                this.Prepare_Window();

                this.Loaded += WpfMainWindow_Loaded;
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void WpfMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                WebBrowser.Load_RR_Url(Common.Resources.URL.login);
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }

        private void MenuItem_SwitchUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }
            catch (Exception ex)
            {
                Log.Error(ex, classGlobalFunctions.Get_CurrentMethod());
            }
        }
    }
}
